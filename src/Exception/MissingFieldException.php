<?php declare(strict_types = 1);

namespace Sigeco\Exception;

use DL2\Slim\Exception;

class MissingFieldException extends Exception
{
    /**
     * @param string[] $fields
     */
    public function __construct(array $fields)
    {
        parent::__construct([
            'fields'  => $fields,
            'message' => 'Validation failed',
            'type'    => 'missing_field',
        ]);
    }
}
