<?php declare(strict_types = 1);

namespace Sigeco\Domain;

use JsonSerializable;

class User implements JsonSerializable
{
    /**
     * @var ?string
     */
    public $id;

    /**
     * @var string
     */
    public $username;

    public function __construct(string $username, ?string $id = null)
    {
        $this->id       = $id;
        $this->username = $username;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'       => $this->id,
            'username' => $this->username,
        ];
    }
}
