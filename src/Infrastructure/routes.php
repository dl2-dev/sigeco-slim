<?php declare(strict_types = 1);

namespace Sigeco\Infrastructure;

use Generator;

/**
 * @return Generator<string,string>
 */
function routes(): Generator
{
    yield '/auth/login' => 'Auth\\Login';
    yield '/auth/logout' => 'Auth\\Logout';
    yield '/auth/refresh-token' => 'Auth\\RefreshToken';
    yield '/auth/reset-password' => 'Auth\\ResetPassword';
    yield '/server/{format:date|time}' => 'Server\\DateTime';
    yield '/server/version' => 'Server\\Version';
}
