<?php declare(strict_types = 1);

namespace Sigeco\Actions\Server;

use Sigeco\Actions\Controller;
use const Sigeco\Infrastructure\VERSION;
use Slim\Http\Request;
use Slim\Http\Response;

class Version extends Controller
{
    protected const REQUIRES_AUTH = false;

    public function get(Request $req, Response $res, array $args): Response
    {
        return $res->withJson(VERSION);
    }
}
