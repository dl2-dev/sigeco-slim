<?php declare(strict_types = 1);

namespace Sigeco\Actions\Auth;

use Sigeco\Actions\Controller;
use Slim\Http\Request;
use Slim\Http\Response;

class Logout extends Controller
{
    public function get(Request $req, Response $res, array $args): Response
    {
        // @todo: revoke the authorization token for real
        return $res;
    }
}
