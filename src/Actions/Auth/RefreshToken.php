<?php declare(strict_types = 1);

namespace Sigeco\Actions\Auth;

use DL2\Slim\Application;
use DL2\Slim\Exception;
use DL2\Slim\Utils\JWT;
use RT7\Table\Sigue\Usuario;
use Sigeco\Actions\Controller;
use Sigeco\Exception\MissingFieldException;
use Slim\Http\Request;
use Slim\Http\Response;

class RefreshToken extends Controller
{
    /**
     * ### Parameters
     *  - password: string.
     *  - token: string.
     *
     * ### Response
     */
    public function post(Request $req, Response $res): Response
    {
        /** @var string */
        $password = $req->getParsedBodyParam('password');

        if (!$password) {
            throw new MissingFieldException(['password']);
        }

        /** @var object{email:string,type:string,username:string} */
        $identity = Application::getInstance()->getIdentity();

        /** @var ?\RT7\Zend\Db\Table\Row */
        $user = (new Usuario())->fetchRow([
            'login =?' => $identity->username,
            'senha =?' => \md5($password),
        ]);

        if (!$user) {
            $error = [
                'message' => 'Não foi possível encontrar sua conta.',
                'type'    => 'unauthorized',
            ];

            throw new Exception($error);
        }

        $token = JWT::encode([
            'email'    => $identity->email,
            'type'     => $identity->type,
            'username' => $identity->username,
        ]);

        return $res->withJson([
            'token' => $token,
        ]);
    }
}
