<?php declare(strict_types = 1);

namespace Sigeco\Actions\Auth;

use DL2\Slim\Exception;
use DL2\Slim\Utils\JWT;
use RT7\Table\Sigue\Usuario;
use Sigeco\Actions\Controller;
use Sigeco\Exception\MissingFieldException;
use Slim\Http\Request;
use Slim\Http\Response;

class Login extends Controller
{
    protected const REQUIRES_AUTH = false;

    /**
     * ### Parameters
     *  - password: string.
     *  - type?: string.
     *  - username: string.
     *
     * ### Response
     */
    public function post(Request $req, Response $res): Response
    {
        /** @var string */
        $password = $req->getParsedBodyParam('password');

        /** @var string */
        $username = $req->getParsedBodyParam('username');

        if (!$password || !$username) {
            throw new MissingFieldException(['password', 'username']);
        }

        /** @var 'email'|'login' */
        $loginColumn = 'email';

        if (false === \strpos($username, '@')) {
            $loginColumn = 'login';
        }

        /** @var string */
        $type = $req->getParsedBodyParam('type', 'user');

        /** @var ?\RT7\Zend\Db\Table\Row */
        $user = (new Usuario())->fetchRow(["{$loginColumn} =?" => $username]);

        if (!$user) {
            $error = [
                'message' => 'Não foi possível encontrar sua conta.',
                'type'    => 'unauthorized',
            ];

            throw new Exception($error);
        }

        if ($user->senha !== \md5($password)) {
            $error = [
                'message' => 'Senha incorreta.',
                'type'    => 'unauthorized',
            ];

            throw new Exception($error);
        }

        $token = JWT::encode([
            'email'    => $user->email,
            'type'     => $type,
            'username' => $user->login,
        ]);

        return $res->withJson([
            'token' => $token,
        ]);
    }
}
